import React, { useRef, useState } from "react";
import "./Card.css";

interface Todo {
  id: string;
  text: string;
}
export default function Card() {
  // ! Initialize Empty List
  const [list, setList] = useState<Todo[]>([]);

  const inputRef = useRef<HTMLInputElement>(null);

  // ! Add New Todo to the list when clicking on the button
  const addToList = () => {
    const textValue = inputRef.current!.value;
    if (textValue?.trim().length === 0) {
      return;
    }
    setList((prevList: Todo[]) => {
      return [...prevList, { text: textValue, id: Math.random().toString() }];
    });
    inputRef.current!.value = "";
  };

  return (
    <div className="card">
      <h1 className="title">Todo List</h1>
      {list.length > 0 ? (
        <ul className="list">
          {list.map((item, index) => (
            <li className="list-item" key={item.id}>
              {index + 1}. {item.text}
            </li>
          ))}
        </ul>
      ) : (
        <p className="no-items">NO ITEMS !</p>
      )}

      <div className="inp-btn">
        <input type="text" ref={inputRef} />
        <button onClick={addToList}>Add</button>
      </div>
    </div>
  );
}
